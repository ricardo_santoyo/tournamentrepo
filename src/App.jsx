import React from "react";
import "./App.css";
import { createStyles, makeStyles } from "@material-ui/core/styles";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { ThemeProvider } from "@material-ui/styles";
import theme from "./themeConfig";
import Home from "./components/Home";
import { Modal, Fade, Container, Backdrop } from "@material-ui/core";

import { useDispatch, useSelector } from "react-redux";
import { modalAction } from "./redux/TournamentDuck";
import AddTeam from "./components/AddTeam";
import AddResult from "./components/AddResult";
import LeaderBoard from "./components/LeaderBoard";

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            margin: theme.spacing(0),
            padding: theme.spacing(0),
        },
        modal: {
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
        },
        paper: {
            backgroundColor: theme.palette.background.paper,
            border: "1px solid lightgrey",
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
        },
    })
);

function App() {
    const classes = useStyles();
    const { modal } = useSelector((store) => store.tournamentReducer);
    const dispatch = useDispatch();
    const handleClose = () => {
        dispatch(modalAction(false));
    };
    return (
        <ThemeProvider theme={theme}>
            <div>
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={modal.active}
                    onClose={handleClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}>
                    <Fade in={modal.active}>
                        <div className={classes.paper}>
                            {modal.screen === "team" ? <AddTeam /> : modal.screen === "match" ? <AddResult /> : <LeaderBoard />}
                        </div>
                    </Fade>
                </Modal>
            </div>
            <Router>
                <Container className={classes.root} maxWidth="xl">
                    <Switch>
                        <Route component={Home} path="/" exact />
                        <Route component={Home} path="/:isAdmin" exact />
                    </Switch>
                </Container>
            </Router>
        </ThemeProvider>
    );
}

export default App;
