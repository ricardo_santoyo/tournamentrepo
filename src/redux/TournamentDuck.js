//constants
import {getDBTeams, getDBMatches} from "../strapiGraphql";


const intialData = {
    modal: { active: false, Screen: "" },
    user: { isAdmin: true },
    error: false,
    teams: [
        // { name: "Dresden", color: "red" },
        // { name: "Treffen", color: "blue" },
        // { name: "Xplode", color: "green" },
        // { name: "Yul", color: "purple" },
        // { name: "Horses", color: "cyan" },
    ],
    matches: [
        // {
        //     p1: { team: { name: "Dresden", color: "red" }, score: 3 },
        //     p2: { team: { name: "Treffen", color: "blue" }, score: 4 },
        // },
        // {
        //     p1: { team: { name: "Dresden", color: "green" }, score: 5 },
        //     p2: { team: { name: "Xplode", color: "cyan" }, score: 2 },
        // },
    ],
    editMatch: {},
};

//type
const GET_MODAL = "GET_MODAL";
const SET_MATCHES = "SET_MATCHES";
const SET_TEAMS = "SET_TEAMS";
const EDIT_ADD_MATCH = "EDIT_ADD_MATCH ";
const ADD_TEAM = "ADD_TEAM";
const SET_ADMIN = "SET_ADMIN";

//reducer
export default function tournamentReducer(state = intialData, action) {
    switch (action.type) {
        case GET_MODAL:
            return { ...state, modal: action.payload };
        case SET_MATCHES:
            return { ...state, matches: action.payload };
        case ADD_TEAM:
            return { ...state, teams: action.payload };
        case EDIT_ADD_MATCH:
            return { ...state, editMatch: action.payload };
        case SET_ADMIN:
            return { ...state, user: action.payload };
            case SET_TEAMS:
                return { ...state, teams:action.payload};
        default:
            return state;
    }
}

//action


export const matchToEdit = (match) => async (dispatch, getState) => {
    
    try {
        dispatch({
            type: EDIT_ADD_MATCH,
            payload: match,
        });
    } catch (error) {}
};

export const modalAction = (active, screen) => async (dispatch, getState) => {
    const { modal } = getState().tournamentReducer;
    try {
        dispatch({
            type: GET_MODAL,
            payload: { ...modal, active: active, screen: screen },
        });
    } catch (error) {}
};

export const addTeamAction = (team) => async (dispatch, getState) => {
    const { teams } = getState().tournamentReducer;
    

    try {
        dispatch({
            type: ADD_TEAM,
            payload: [...teams, team],
        });
    } catch (error) {}
};

export const setTeams = () => async (dispatch, getState) => {
    let graphQuery =[];
    const { teams } = getState().tournamentReducer;
    
    try {
         graphQuery = await getDBTeams();
            dispatch({
                type: SET_TEAMS,
                payload: graphQuery ? graphQuery.signiflyTeams: teams,
            });
            dispatch(setAvalableMatches());
        
    } catch (error) {
        
    }

}

export const setAvalableMatches = () => async (dispatch, getState) => {
    const { teams } = getState().tournamentReducer;
   // const { matches } = getState().tournamentReducer;

    let tempMatches = [];
    try {
        let graphQuery =[];
        graphQuery = await getDBMatches();
  
        const matches = graphQuery ? graphQuery.matches : [];

        console.log(matches)       

        teams.forEach((team, i, teamArray) => {
            
            teamArray
                .filter((x) => x.name !== team.name)
                .forEach((rival) => {

                    const currentMatch = matches.filter((match) => {
                        return match.p1.signifly_team.name === team.name && match.p2.signifly_team.name === rival.name
                    });
                    tempMatches = [
                        ...tempMatches,
                        !currentMatch[0]
                            ? { p1: { signifly_team: team, score: null }, p2: { signifly_team: rival, score: null } }
                            : { p1: { signifly_team: team, score: currentMatch[0].p1.Score }, p2: { signifly_team: rival, score: currentMatch[0].p2.Score } },
                    ];
                });
        });

        dispatch({
            type: SET_MATCHES,
            payload: tempMatches,
        });
    } catch (error) {}
};
