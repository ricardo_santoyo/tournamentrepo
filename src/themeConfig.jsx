//import {createMuiTheme} from "@material-ui/core/styles";
import { unstable_createMuiStrictModeTheme as createMuiTheme } from "@material-ui/core/styles";
import { grey } from "@material-ui/core/colors";

const theme = createMuiTheme({

    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: grey[900],
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
           
            main: '#F5AA2B',
           //contrastText: "#ffcc00",
        },
        info: {
          light: "#fafafa",
          main: "#fafafa",
          dark: "#666",
          contrastText: "red",
        },

        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
    },
    typography: {
          h5: {
           
            fontWeight:'bold',
            fontSize: "1rem",
            textTransform: 'uppercase'
        },
        h6: {
           // color: "#fff",
            fontWeight:'bold',
            fontSize: "1.5rem",
            textTransform: 'uppercase'
        },
        
    },
});
export default theme;
