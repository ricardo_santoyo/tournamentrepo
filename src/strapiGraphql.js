import Strapi from "strapi-sdk-javascript";
const apiUrl = process.env.API_URL || "http://www.paleggburgeradmin.site";
const strapi = new Strapi(apiUrl);



async function getDBTeams() {
    try {
        const graphQuery = await strapi.request("POST", "/graphql", {
            data: {
                query: ` 
                {
                  signiflyTeams{
                    name, color
                  }
                }
                `,               
            },
        });
        return graphQuery.data;
    } catch (error) {}
}

async function getDBMatches() {
  try {
      const graphQuery = await strapi.request("POST", "/graphql", {
          data: {
              query: ` 
              {
                matches{
                  p1{
                    Score,
                    signifly_team {
                      name, color
                    }
                  },
                  p2{
                    Score,
                    signifly_team {
                      name, color
                    }
                  }
                }
              }
              `,               
          },
      });
      return graphQuery.data;
  } catch (error) {}
}



export { getDBTeams, getDBMatches };



