import React from "react";
import { Grid, Typography, Box, FormControl, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import { modalAction } from "../redux/TournamentDuck";

const useStyle = makeStyles({
    root: {
        display: "flex",
        flexDirection: "column",
        minHeight: "100%",
    },
});

const AddResult = () => {
    const classes = useStyle();

    const { editMatch } = useSelector((store) => store.tournamentReducer);
    const [error, setError] = React.useState(null);
    const dispatch = useDispatch();
    const [teamScore, setTeamScore] = React.useState(null);
    const [rivalScore, setRivalScore] = React.useState(null);

    React.useEffect(() => {
        setTeamScore(editMatch.p1.score);
        setRivalScore(editMatch.p2.score);
    }, [editMatch.p1.score, editMatch.p2.score]);

    const handleSubmit = (event) => {
        event.preventDefault();
        setError(null);

        if (teamScore === null || teamScore < 0 || rivalScore === null || rivalScore < 0 || teamScore===rivalScore) {
            setError("Select a valid score");
            return;
        }

        const matchToUdapte = {...editMatch};
        matchToUdapte.p1.score=teamScore;
        matchToUdapte.p2.score=rivalScore;

        event.target.reset();
        //dispatch(updateMatch(matchToUdapte));
        dispatch(modalAction(false));
    };

    return (
        <Grid className={classes.root}>
            <Typography variant="h5" color="primary">
                ADD/UPDATE RESULT
            </Typography>
            <form onSubmit={handleSubmit}>
                <FormControl>
                    <Box display="flex" flexDirection="row">
                        <Box bgcolor={editMatch.p1.signifly_team.color}>&nbsp;</Box>
                        <TextField
                            id="filled-helperText"
                            label={editMatch.p1.signifly_team.name}
                            type="number"
                            defaultValue={teamScore}
                            variant="filled"
                            value={teamScore !== null && teamScore}
                            onChange={(e) => setTeamScore(e.target.value)}
                        />
                    </Box>

                    <Box display="flex" flexDirection="row">
                        <Box bgcolor={editMatch.p2.signifly_team.color}>&nbsp;</Box>
                        <TextField
                            id="filled-helperText"
                            label={editMatch.p2.signifly_team.name}
                            type="number"
                            defaultValue={rivalScore}
                            variant="filled"
                            value={rivalScore !== null && rivalScore}
                            onChange={(e) => setRivalScore(e.target.value)}
                        />
                    </Box>
                    {error && (
                        <Typography variant="h5" color="error">
                            {error}
                        </Typography>
                    )}
                    <input type="submit" value="Submit" />
                </FormControl>
            </form>
        </Grid>
    );
};

export default AddResult;
