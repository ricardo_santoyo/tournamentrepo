import React from "react";
import { Grid, Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import theme from "./../themeConfig";
import Matches from "./Matches";
import { useSelector, useDispatch } from "react-redux";
import { setAvalableMatches, setTeams } from "./../redux/TournamentDuck";



const useStyle = makeStyles({
    root: {
        //fontColor: theme.palette.primary.main,
        height: "100vh",
        marging: 0,
    },
});
const Home = (theme) => {
    const classes = useStyle();

    const { teams } = useSelector((store) => store.tournamentReducer);
    const { matches } = useSelector((store) => store.tournamentReducer);

    const dispatch = useDispatch();

   

    React.useEffect(() => {
        
       
        dispatch(setTeams());
        //dispatch(setAvalableMatches());
    }, [dispatch]);

    return (
        <Grid className={classes.root}>
            
            <Box height='5%' bgcolor='#90E3B6'><Typography variant="h6" color="primary" align='center'>SIGNIFLY FOOSBALL TOURNAMENT</Typography></Box>
            <Matches height='90%' matches={matches} teams={teams} />
            <Box height='5%' bgcolor='#90E3B6'>&nbsp;</Box>
        </Grid>
    );
};

export default Home;
