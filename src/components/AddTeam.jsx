import React from "react";
import { Grid, Typography, Box, FormControl, InputLabel, Input } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { modalAction, addTeamAction, setAvalableMatches } from "../redux/TournamentDuck";
import { useDispatch } from "react-redux";
import { SketchPicker } from "react-color";

const useStyle = makeStyles({
    root: {
        display: "flex",
        flexDirection: "column",
        minHeight: "100%",
    },
});

const Matches = () => {
    const classes = useStyle();

    const [team, setTeam] = React.useState({ name: "", color: "" });

    const [error, setError] = React.useState(null);
    const dispatch = useDispatch();

    const handleChangeComplete = (color) => {
        setTeam({ ...team, color: color.hex });
    };

    const handleSubmit = (event) => {
       
        event.preventDefault();
        setError(null);

        if (!team.name.trim() && team.name.length < 8) {
            setError("Please select a color");
            return;
        }
        if (!team.color.trim()) {
            setError("Please use a valid name");
            return;
        }

        event.target.reset();
        dispatch(addTeamAction(team));
        dispatch(modalAction(false));
        dispatch(setAvalableMatches());
    };

    return (
        <Grid className={classes.root}>
            <Typography variant="h5" color="primary">
                ADD A TEAM
            </Typography>
            <form onSubmit={handleSubmit}>
                <FormControl>
                    <InputLabel htmlFor="my-input" color="primary">
                        Team Name
                    </InputLabel>
                    <Input id="my-input" aria-describedby="my-helper-text" value={team.name} onChange={(e) => setTeam({ ...team, name: e.target.value })} />
                    <Box m={2} />
                    <Typography variant="h5" color="primary">
                        Team Color
                    </Typography>
                    <SketchPicker color={team.color} onChangeComplete={handleChangeComplete} />
                    {error && (
                        <Typography variant="h5" color="error">
                            {error}
                        </Typography>
                    )}

                    <input type="submit" value="Submit" />
                </FormControl>
            </form>
        </Grid>
    );
};

export default Matches;
