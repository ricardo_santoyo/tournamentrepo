import React from "react";
import { Grid, Typography, Box, Hidden } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import theme from "./../themeConfig";
import { modalAction, matchToEdit } from "../redux/TournamentDuck";
import { useDispatch, useSelector } from "react-redux";
import BarChartIcon from "@material-ui/icons/BarChart";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";


const useStyle = makeStyles({
    root: {
        display: "flex",
        flexDirection: "column",
        height: "90%",
    },
    row: {
        display: "flex",
        flexDirection: "row",
        //backgroundColor: "blue",
        flexGrow: "1",
    },
    cell: {
        display: "flex",
        flexGrow: "1",
        flexBasis: "0",
        margin: theme.spacing(0.2),
        alignItems: "center",
        justifyContent: "center",
        bgcolor: theme.palette.primary,
        //minWidth: "4ch",
    },
    cellTile: {
        border: "solid",
        borderWidth: "0.1rem",
        borderColor: "lightGray",
    },
    listContainer: {
        display: "flex",
        flexDirection: "column",
        height: "100%",
        justifyContent: "space-evenly",
        flex: "1",
    },
});

const Matches = (props, theme) => {
    const classes = useStyle();

    const { user } = useSelector((store) => store.tournamentReducer);

    const matchResult = (teamA, teamB, resultTeam) => {
        const match = props.matches.filter((x) => x.p1.signifly_team.name === teamA.name && x.p2.signifly_team.name === teamB.name)[0];
        if (match && resultTeam === 0) return match.p1.score;
        else if (match && resultTeam === 1) return match.p2.score;
        else return null;
    };

    const dispatch = useDispatch();

    const clickHandlerAddTeam = (e) => {
        dispatch(modalAction(true, "team"));
    };

    const addResult = (teamA, teamB) => {
        if (user.isAdmin) {
            dispatch(matchToEdit(props.matches.filter((x) => x.p1.signifly_team.name === teamA.name && x.p2.signifly_team.name === teamB.name)[0]));
            dispatch(modalAction(true, "match"));
        }

        //
    };
    const checkLeaderBoard = () => {
        dispatch(modalAction(true, "leaderBoard"));
    };

   

    return (
        <Grid className={classes.root}>
            {/* row print */}

            <Hidden smUp>
                <Box display="flex" flexDirection="row">
                    {props.teams.length < 8 && user.isAdmin && (
                        <Box className={classes.cell} onClick={clickHandlerAddTeam} maxHeight="5rem">
                            <AddCircleOutlineIcon color="primary" />
                        </Box>
                    )}
                    <Box className={classes.cell} onClick={checkLeaderBoard} maxHeight="5rem">
                        <BarChartIcon color="primary" />
                    </Box>
                </Box>

                {props.teams.map((tRow, i, teamArray) => (
                    <Grid key={tRow + "_" + i} className={classes.listContainer}>
                        {teamArray.map(
                            (team) =>
                                tRow.name !== team.name && (
                                    <Box
                                        key={team.name}
                                        className={classes.cell}
                                        bgcolor="info.light"
                                        justifyContent="center"
                                        onClick={() => addResult(tRow, team)}>
                                        <Box display="flex" flexDirection="row" justifyContent="flex-end" width="50%">
                                            <Typography variant="h5" color="primary">
                                                {tRow.name}
                                            </Typography>
                                            <Box bgcolor={team.color} width="0.2rem" marginLeft={1}>
                                                &nbsp;
                                            </Box>
                                            <Box width="1rem" marginLeft={1}>
                                                <Typography variant="h5" color="primary">
                                                    {matchResult(tRow, team, 0)}
                                                </Typography>
                                            </Box>
                                        </Box>

                                        <Box display="flex" flexDirection="row" justifyContent="flex-start" width="50%">
                                            <Box width="1rem">
                                                <Typography variant="h5" color="primary">
                                                    {matchResult(tRow, team, 1)}
                                                </Typography>
                                            </Box>
                                            <Box bgcolor={team.color} width="0.2rem" marginRight={1}>
                                                &nbsp;
                                            </Box>
                                            <Typography variant="h5" color="primary">
                                                {team.name}
                                            </Typography>
                                        </Box>
                                    </Box>
                                )
                        )}
                    </Grid>
                ))}
            </Hidden>
            {/* Grid print */}
            <Hidden only="xs">
                <Grid className={classes.row}>
                    <Box className={`${classes.cell} ${classes.cellTile}`} bgcolor="lightGray" display='flex' flexDirection='column' onClick={user.isAdmin && clickHandlerAddTeam}>
                    {props.teams.length < 2 && user.isAdmin && <Typography variant="h6" color="primary">ADD A TEAM</Typography>}
                        {props.teams.length < 8 && user.isAdmin && <AddCircleOutlineIcon color="primary" />}
                    </Box>
                    {props.teams.map((tRow) => (
                        <Box key={tRow.name} className={`${classes.cell} ${classes.cellTile}`} display="flex" flexDirection="column">
                            <Typography variant="h6" color="primary">
                                <Hidden mdDown>{tRow.name.substring(0, 8)}</Hidden>
                                <Hidden lgUp>{tRow.name.substring(0, 3)}</Hidden>
                            </Typography>
                            <Box bgcolor={tRow.color} width="50%" height="0.25rem">
                                &nbsp;
                            </Box>
                        </Box>
                    ))}
                </Grid>
                {props.teams.map((tRow, i, teamArray) => (
                    <Grid key={tRow.name} className={classes.row}>
                        <Box className={`${classes.cell} ${classes.cellTile}`} minWidth="4ch" display="flex" flexDirection="column">
                            <Typography variant="h6" color="primary">
                                <Hidden mdDown>{tRow.name.substring(0, 8)}</Hidden>
                                <Hidden lgUp>{tRow.name.substring(0, 3)}</Hidden>
                            </Typography>
                            <Box bgcolor={tRow.color} width="50%" height="0.25rem">
                                &nbsp;
                            </Box>
                        </Box>

                        {teamArray.map((team) => (
                            <Box
                                key={team.name}
                                className={`${classes.cell} ${classes.cellTile}`}
                                bgcolor={tRow.name === team.name ? "lightGray" : ""}
                                minWidth="4ch">
                                {matchResult(tRow, team, 0) ? (
                                    <Box
                                        direction="row"
                                        display="flex"
                                        justifyContent="center"
                                        alignContent="center"
                                        bgcolor="#f0f0f0"
                                        width="100%"
                                        height="100%"
                                        onClick={() => addResult(tRow, team)}>
                                        <Box className={classes.cell} flexDirection="column" padding={1}>
                                            <Box>
                                                <Typography variant="h6" color="primary">
                                                    {matchResult(tRow, team, 0)}
                                                </Typography>
                                            </Box>
                                            <Box width="100%" height="0.25rem" bgcolor={tRow.color}>
                                                {" "}
                                            </Box>
                                        </Box>
                                        <Box className={classes.cell} flexDirection="column" padding={1}>
                                            <Box>
                                                {" "}
                                                <Typography variant="h6" color="primary">
                                                    {matchResult(tRow, team, 1)}
                                                </Typography>
                                            </Box>
                                            <Box width="100%" height="0.25rem" bgcolor={team.color}>
                                                {" "}
                                            </Box>
                                        </Box>
                                    </Box>
                                ) : tRow.name !== team.name ? (
                                    <Box
                                        onClick={() => user.isAdmin && addResult(tRow, team)}
                                        display="flex"
                                        bgcolor="#f0f0f0"
                                        width="100%"
                                        height="100%"
                                        justifyContent="center"
                                        alignItems="center">
                                        {user.isAdmin && <AddCircleOutlineIcon color="secondary" />}
                                    </Box>
                                ) : (
                                    <Box onClick={checkLeaderBoard} display="flex" width="100%" height="100%" justifyContent="center" alignItems="center">
                                        <BarChartIcon color="primary" />
                                    </Box>
                                )}
                            </Box>
                        ))}
                    </Grid>
                ))}
            </Hidden>
        </Grid>
    );
};

export default Matches;
