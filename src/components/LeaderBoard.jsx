import React from "react";
import { useSelector } from "react-redux";
import { Typography, Box, TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles({
    root: {
        maxWidth: "90vw",
    },
    table: {
        minWidth: 350,
    },
});

const LeaderBoard = () => {
    const classes = useStyle();
    const { teams } = useSelector((store) => store.tournamentReducer);
    const { matches } = useSelector((store) => store.tournamentReducer);

    const [boardData, setBoardData] = React.useState([]);

    React.useEffect(() => {
        const teamMatches = teams.map((team) => {
            return {
                name: team.name,
                matches: matches.filter((match) => (match.p1.signifly_team.name === team.name || match.p2.signifly_team.name === team.name) && match.p1.score),
            };
        });

        setBoardData(
            teamMatches.map((tm) => {
                return {
                    name: tm.name,
                    playedGames: tm.matches.length,
                    winMatches: tm.matches.filter(
                        (match) =>
                            (match.p1.signifly_team.name === tm.name && match.p1.score > match.p2.score) ||
                            (match.p2.signifly_team.name === tm.name && match.p2.score > match.p1.score)
                    ).length,
                };
            }).sort((a,b) => b.winMatches - a.winMatches)
        );
    }, [matches, teams]);

    return (
        <Box className={classes.root}>
            <Box m={1}>
                <Typography variant="h6" color="primary">
                    LEADERBOARD
                </Typography>
            </Box>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Team</TableCell>
                            <TableCell align="right">Played</TableCell>
                            <TableCell align="right">Wins</TableCell>
                            <TableCell align="right">Pts</TableCell>
                            <TableCell align="right">Rank</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {boardData.map((row, i) => (
                            <TableRow key={row.name}>
                                <TableCell component="th" scope="row">
                                    <Box display="flex" flexDirection="row">
                                        <Box width="0.25rem" mr={1} bgcolor={teams.filter((x) => x.name === row.name)[0].color}>
                                            &nbsp;
                                        </Box>
                                        {row.name}
                                    </Box>
                                </TableCell>
                                <TableCell align="right">{row.playedGames}</TableCell>
                                <TableCell align="right">{row.winMatches}</TableCell>
                                <TableCell align="right">{row.winMatches * 3}</TableCell>
                                <TableCell align="right">{i + 1}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

        </Box>
    );
};

export default LeaderBoard;
